"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
// https://forum.ionicframework.com/t/inserting-html-via-angular-2-use-of-domsanitizationservice-bypasssecuritytrusthtml/62562/5
var SafePipe = (function () {
    function SafePipe(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    SafePipe.prototype.transform = function (value, type) {
        if (type === void 0) { type = 'html'; }
        switch (type) {
            case 'html': return this._sanitizer.bypassSecurityTrustHtml(value);
            case 'style': return this._sanitizer.bypassSecurityTrustStyle(value);
            case 'background-image': return this._sanitizer.bypassSecurityTrustStyle("url('" + value + "')");
            case 'youtu-background-image':
                var url = value.split('/');
                return this._sanitizer.bypassSecurityTrustStyle("url('https://i.ytimg.com/vi/" + url[url.length - 1] + "/hqdefault.jpg')"); // https://i.ytimg.com/vi/HSOtku1j600/hqdefault.jpg
            case 'script': return this._sanitizer.bypassSecurityTrustScript(value);
            case 'url': return this._sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this._sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error("Invalid safe type specified: " + type);
        }
    };
    SafePipe.decorators = [
        { type: core_1.Pipe, args: [{
                    name: 'safe'
                },] },
    ];
    /** @nocollapse */
    SafePipe.ctorParameters = function () { return [
        { type: platform_browser_1.DomSanitizer, },
    ]; };
    return SafePipe;
}());
exports.SafePipe = SafePipe;
//# sourceMappingURL=safe.pipe.js.map