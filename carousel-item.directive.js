"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CarouselItemDirective = (function () {
    function CarouselItemDirective() {
        this.clickEvent = new core_1.EventEmitter();
    }
    CarouselItemDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[carousel-item]'
                },] },
    ];
    /** @nocollapse */
    CarouselItemDirective.ctorParameters = function () { return []; };
    CarouselItemDirective.propDecorators = {
        "clickEvent": [{ type: core_1.Output, args: ['carousel-item-click',] },],
    };
    return CarouselItemDirective;
}());
exports.CarouselItemDirective = CarouselItemDirective;
//# sourceMappingURL=carousel-item.directive.js.map