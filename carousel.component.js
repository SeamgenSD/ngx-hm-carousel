"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/observable/fromEvent");
require("rxjs/add/observable/interval");
require("rxjs/add/operator/bufferCount");
require("rxjs/add/operator/debounceTime");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
require("rxjs/add/operator/merge");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/takeUntil");
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var Observable_1 = require("rxjs/Observable");
var Subject_1 = require("rxjs/Subject");
var carousel_item_directive_1 = require("./carousel-item.directive");
var common_1 = require("@angular/common");
// if the pane is paned .25, switch to the next pane.
var PANBOUNDARY = 0.15;
var RUN_DIRECTION;
(function (RUN_DIRECTION) {
    RUN_DIRECTION["LEFT"] = "left";
    RUN_DIRECTION["RIGHT"] = "right";
})(RUN_DIRECTION = exports.RUN_DIRECTION || (exports.RUN_DIRECTION = {}));
var CarouselComponent = (function () {
    function CarouselComponent(platformId, _renderer) {
        this.platformId = platformId;
        this._renderer = _renderer;
        this.infinite = false;
        this.mourseEnable = false;
        this.speed = 5000;
        this.delay = 8000;
        this.direction = RUN_DIRECTION.RIGHT;
        this._showNum = 1;
        this.isAutoNum = false;
        this.scrollNum = 1;
        this.isDragMany = false;
        this._viewIndex = 0;
        this._autoplay = false;
        this.indexChanged = new core_1.EventEmitter();
        this._porgressWidth = 0;
        this.elmWidth = 0;
        this.isInContainer = false;
        this.restart = new BehaviorSubject_1.BehaviorSubject(null);
        this.stopEvent = new Subject_1.Subject();
        this.mostRightIndex = 0;
    }
    Object.defineProperty(CarouselComponent.prototype, "showNum", {
        set: function (value) {
            if (value === 'auto') {
                this.isAutoNum = true;
                this._showNum = this.getWindowWidthToNum();
            }
            else {
                this._showNum = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CarouselComponent.prototype, "currentIndex", {
        get: function () {
            return this._viewIndex;
        },
        set: function (value) {
            this._viewIndex = value;
            if (this.itemsElm) {
                this.drawView(this._viewIndex);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CarouselComponent.prototype, "autoplay", {
        get: function () {
            return this._autoplay;
        },
        set: function (value) {
            if (this.itemsElm) {
                this.progressWidth = 0;
                if (value) {
                    this.sub$ = this.doNext.subscribe();
                }
                else {
                    if (this.sub$) {
                        this.sub$.unsubscribe();
                    }
                }
            }
            this._autoplay = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CarouselComponent.prototype, "progressWidth", {
        get: function () {
            return this._porgressWidth;
        },
        set: function (value) {
            if (this.progressElm !== undefined && this.autoplay) {
                this._porgressWidth = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    CarouselComponent.prototype.onResize = function (event) {
        this.setViewWidth();
        this.drawView(this.currentIndex);
    };
    CarouselComponent.prototype.ngAfterContentInit = function () {
        this.initVariable();
        // console.log(this.progressElm);
    };
    CarouselComponent.prototype.ngAfterViewInit = function () {
        this.setViewWidth(true);
        if (common_1.isPlatformBrowser(this.platformId)) {
            this.hammer = this.bindHammer();
        }
        this.drawView(this.currentIndex);
        this.bindClick();
    };
    CarouselComponent.prototype.ngOnDestroy = function () {
        if (this.btnNext && this.btnPrev) {
            this.nextListener();
            this.prevListener();
        }
        if (common_1.isPlatformBrowser(this.platformId)) {
            this.hammer.destroy();
        }
        if (this.autoplay) {
            this.sub$.unsubscribe();
        }
    };
    CarouselComponent.prototype.initVariable = function () {
        var _this = this;
        this.rootElm = this.parentChild.nativeElement;
        this.containerElm = this.rootElm.children[0];
        this.itemsElm = Array.from(this.containerElm.children);
        this.mostRightIndex = this.itemsElm.length - this._showNum;
        if (this.dotElm) {
            this.dots = new Array(this.itemsElm.length - (this._showNum - 1)).map(function (x, i) { return i; });
        }
        var startEvent = this.restart; // .merge(this.mourseLeave); // .map(() => console.log('start'))
        var stopEvent = this.stopEvent;
        if (this.mourseEnable) {
            var mourseOver = Observable_1.Observable.fromEvent(this.containerElm, 'mouseover')
                .map(function () {
                _this.isInContainer = true;
                // console.log('over');
            });
            var mourseLeave = Observable_1.Observable.fromEvent(this.containerElm, 'mouseleave')
                .map(function () {
                _this.isInContainer = false;
                // console.log('levae');
            });
            startEvent = startEvent.merge(mourseLeave);
            stopEvent = stopEvent.merge(mourseOver);
        }
        // const debounceTime = this.delay < this.speed ? this.delay : this.delay - this.speed;
        this.doNext = startEvent
            .debounceTime(this.delay)
            .switchMap(function () {
            return _this.runProgress(20)
                .do(function () {
                // console.log('next');
                if (_this.direction === RUN_DIRECTION.LEFT) {
                    _this.currentIndex -= _this.scrollNum;
                }
                else {
                    _this.currentIndex += _this.scrollNum;
                }
            })
                .takeUntil(stopEvent.map(function () {
                // console.log('stop');
                // console.log('stop');
                _this.progressWidth = 0;
            }));
        });
        if (this.autoplay) {
            this.sub$ = this.doNext.subscribe();
        }
    };
    CarouselComponent.prototype.setViewWidth = function (isInit) {
        var _this = this;
        if (this.isAutoNum) {
            this._showNum = this.getWindowWidthToNum();
            this.mostRightIndex = this.itemsElm.length - this._showNum;
            if (this.dotElm) {
                this.dots = new Array(this.itemsElm.length - (this._showNum - 1)).map(function (x, i) { return i; });
            }
        }
        this._renderer.addClass(this.containerElm, 'grab');
        // when init check view has scroll bar
        var totalWidth = 0;
        if (isInit) {
            // remain one elm height
            this._renderer.addClass(this.containerElm, 'hm-carousel-display-npwrap');
        }
        this.elmWidth = (totalWidth + this.rootElm.clientWidth) / this._showNum;
        this._renderer.removeClass(this.containerElm, 'hm-carousel-display-npwrap');
        this._renderer.setStyle(this.containerElm, 'width', this.elmWidth * this.itemsElm.length + "px");
        this._renderer.setStyle(this.containerElm, 'position', 'relative');
        this.itemsElm.forEach(function (elm, index) {
            _this._renderer.setStyle(elm, 'width', _this.elmWidth + "px");
        });
    };
    CarouselComponent.prototype.bindHammer = function () {
        var _this = this;
        var hm = new Hammer(this.containerElm);
        hm.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });
        hm.on('panleft panright panend pancancel tap', function (e) {
            _this._renderer.removeClass(_this.containerElm, 'transition');
            _this._renderer.addClass(_this.containerElm, 'grabbing');
            if (_this.autoplay) {
                _this.stopEvent.next();
            }
            switch (e.type) {
                case 'tap':
                    _this.callClick(e.center.x);
                    _this.callRestart();
                    _this._renderer.removeClass(_this.containerElm, 'grabbing');
                    break;
                case 'panend':
                case 'pancancel':
                    _this._renderer.removeClass(_this.containerElm, 'grabbing');
                // tslint:disable-next-line:no-switch-case-fall-through
                case 'panleft':
                case 'panright':
                    _this.handlePan(e);
                    break;
            }
        });
        return hm;
    };
    CarouselComponent.prototype.bindClick = function () {
        var _this = this;
        if (this.btnNext && this.btnPrev) {
            this.nextListener = this._renderer.listen(this.btnNext.nativeElement, 'click', function () {
                _this.setIndex(_this.currentIndex + 1);
            });
            this.prevListener = this._renderer.listen(this.btnPrev.nativeElement, 'click', function () {
                _this.setIndex(_this.currentIndex - 1);
            });
        }
    };
    CarouselComponent.prototype.callRestart = function () {
        if (this.autoplay && !this.isInContainer) {
            this.restart.next(null);
        }
    };
    CarouselComponent.prototype.callClick = function (positionX) {
        var toIndex = this.currentIndex + Math.floor(positionX / this.elmWidth);
        Array.from(this.items)[toIndex].clickEvent.emit(toIndex);
    };
    CarouselComponent.prototype.drawView = function (index) {
        this._renderer.addClass(this.containerElm, 'transition');
        if (this.autoplay || this.infinite) {
            this.playCycle(index);
        }
        else {
            this._viewIndex = Math.max(0, Math.min(index, this.mostRightIndex));
        }
        // this.containerElm.style.transform = `translate3d(${-this.currentIndex * this.elmWidth}px, 0px, 0px)`;
        this._renderer.setStyle(this.containerElm, 'left', -this.currentIndex * this.elmWidth + "px");
        this.indexChanged.emit(this.currentIndex);
    };
    CarouselComponent.prototype.playCycle = function (index) {
        switch (this.direction) {
            case RUN_DIRECTION.LEFT:
                if (index === -this.scrollNum) {
                    this._viewIndex = this.mostRightIndex;
                }
                else if (index > this.mostRightIndex || index < 0) {
                    this._viewIndex = 0;
                }
                break;
            case RUN_DIRECTION.RIGHT:
                if (index === this.mostRightIndex + this.scrollNum) {
                    this._viewIndex = 0;
                }
                else if (index < 0 || this._viewIndex >= this.mostRightIndex) {
                    this._viewIndex = this.mostRightIndex;
                }
                break;
        }
    };
    CarouselComponent.prototype.handlePan = function (e) {
        // console.log(e.deltaX / this.elmWidth);
        // console.log(moveNum);
        switch (e.type) {
            case 'panleft':
            case 'panright':
                // Slow down at the first and last pane.
                if (this.outOfBound(e.type) && (!this.infinite)) {
                    e.deltaX *= 0.5;
                }
                this._renderer.setStyle(this.containerElm, 'left', -this.currentIndex * this.elmWidth + e.deltaX + "px");
                this.prePanMove = false;
                if (!this.isDragMany) {
                    if (Math.abs(e.deltaX) > this.elmWidth * 0.5) {
                        if (e.deltaX > 0) {
                            this.currentIndex -= this.scrollNum;
                        }
                        else {
                            this.currentIndex += this.scrollNum;
                        }
                        this._renderer.removeClass(this.containerElm, 'grabbing');
                        this.callRestart();
                        this.hammer.stop(true);
                        // remember prv action, to avoid hammer stop, then click
                        this.prePanMove = true;
                    }
                }
                break;
            case 'panend':
            case 'pancancel':
                this.callRestart();
                if (Math.abs(e.deltaX) > this.elmWidth * PANBOUNDARY) {
                    var moveNum = this.isDragMany ?
                        Math.ceil(Math.abs(e.deltaX) / this.elmWidth) : this.scrollNum;
                    if (e.deltaX > 0) {
                        this.currentIndex -= moveNum;
                    }
                    else {
                        this.currentIndex += moveNum;
                    }
                    break;
                }
                else {
                    if (!this.isDragMany && this.prePanMove) {
                        this.callClick(e.center.x);
                    }
                }
                this.drawView(this.currentIndex);
                this.prePanMove = false;
                break;
        }
    };
    CarouselComponent.prototype.setIndex = function (index) {
        if (this.autoplay) {
            this.stopEvent.next();
            this.restart.next('do restart');
        }
        this.currentIndex = index;
    };
    CarouselComponent.prototype.outOfBound = function (type) {
        switch (type) {
            case 'panleft':
                return this.currentIndex === this.mostRightIndex;
            case 'panright':
                return this.currentIndex === 0;
        }
    };
    CarouselComponent.prototype.runProgress = function (betweenTime) {
        var _this = this;
        var howTimes = this.speed / betweenTime;
        var everyIncrease = 100 / this.speed * betweenTime;
        // console.log('progress');
        return Observable_1.Observable.interval(betweenTime)
            .map(function (t) {
            // console.log(t % howTimes);
            // const persent = ;
            // console.log(t % howTimes);
            // const persent = ;
            _this.progressWidth = (t % howTimes) * everyIncrease;
        })
            .bufferCount(Math.round(this.speed / betweenTime), 0);
    };
    CarouselComponent.prototype.getWindowWidthToNum = function () {
        var initNum = 3;
        // 610
        // if use window do check to avoid ssr problem.
        if (window) {
            var windowWidth = window.innerWidth;
            if (windowWidth > 300) {
                return Math.floor(initNum + (windowWidth / 200));
            }
        }
        return initNum;
    };
    CarouselComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'carousel-container',
                    template: "\n    <div #parentChild class=\"carousel\">\n      <ng-content select=\"[carousel-items-container]\"></ng-content>\n      <!-- left -->\n      <div #prev *ngIf=\"contentPrev\"\n        class=\"direction left\">\n        <ng-container *ngTemplateOutlet=\"contentPrev\"></ng-container>\n      </div>\n      <!--  right -->\n      <div #next *ngIf=\"contentNext\"\n        class=\"direction right\">\n        <ng-container *ngTemplateOutlet=\"contentNext\"></ng-container>\n      </div>\n      <!-- indicators -->\n      <ul class=\"indicators\" *ngIf=\"dotElm\">\n        <li\n          *ngFor=\"let dot of dots; let i = index;\"\n          (click)=\"setIndex(i)\">\n          <ng-container\n            *ngTemplateOutlet=\"dotElm, context: {\n              $implicit: {\n                index : i,\n                currentIndex : currentIndex\n              }\n            }\">\n          </ng-container>\n        </li>\n      </ul>\n      <!-- progress -->\n      <div *ngIf=\"progressElm && autoplay\">\n        <ng-container\n          *ngTemplateOutlet=\"progressElm, context: {\n            $implicit: {\n              progress:progressWidth\n            }\n          }\">\n        </ng-container>\n      </div>\n    </div>\n  ",
                    styles: ["\n    @charset \"UTF-8\";.hm-carousel-display-npwrap{display:flex!important;flex-wrap:nowrap!important;flex-direction:row!important}.carousel{overflow:hidden;position:relative}.carousel ul.indicators{list-style:none;bottom:1rem;left:0;margin:0;padding:0;position:absolute;text-align:center;width:100%;z-index:2}.carousel ul.indicators li{cursor:pointer;display:inline-block;position:relative;padding:.5rem}.carousel .direction{position:absolute;height:100%;display:flex;align-items:center;justify-content:center;top:0}.carousel .direction.left{left:0;z-index:2}.carousel .direction.right{position:absolute;right:0;z-index:2}.transition{transition:left .4s ease-in-out}.grab{cursor:grab}.grabbing{cursor:grabbing}.pointer{cursor:pointer}\n  "],
                    encapsulation: core_1.ViewEncapsulation.None
                },] },
    ];
    /** @nocollapse */
    CarouselComponent.ctorParameters = function () { return [
        { type: Object, decorators: [{ type: core_1.Inject, args: [core_1.PLATFORM_ID,] },] },
        { type: core_1.Renderer2, },
    ]; };
    CarouselComponent.propDecorators = {
        "parentChild": [{ type: core_1.ViewChild, args: ['parentChild',] },],
        "btnPrev": [{ type: core_1.ViewChild, args: ['prev',] },],
        "btnNext": [{ type: core_1.ViewChild, args: ['next',] },],
        "items": [{ type: core_2.ContentChildren, args: [carousel_item_directive_1.CarouselItemDirective,] },],
        "contentPrev": [{ type: core_1.ContentChild, args: ['carouselPrev',] },],
        "contentNext": [{ type: core_1.ContentChild, args: ['carouselNext',] },],
        "dotElm": [{ type: core_1.ContentChild, args: ['carouselDot',] },],
        "progressElm": [{ type: core_1.ContentChild, args: ['carouselProgress',] },],
        "infinite": [{ type: core_1.Input, args: ['infinite',] },],
        "mourseEnable": [{ type: core_1.Input, args: ['mourse-enable',] },],
        "speed": [{ type: core_1.Input, args: ['autoplay-speed',] },],
        "delay": [{ type: core_1.Input, args: ['between-delay',] },],
        "direction": [{ type: core_1.Input, args: ['autoplay-direction',] },],
        "showNum": [{ type: core_1.Input, args: ['show-num',] },],
        "scrollNum": [{ type: core_1.Input, args: ['scroll-num',] },],
        "isDragMany": [{ type: core_1.Input, args: ['drag-many',] },],
        "currentIndex": [{ type: core_1.Input, args: ['current-index',] },],
        "autoplay": [{ type: core_1.Input, args: ['autoplay',] },],
        "indexChanged": [{ type: core_1.Output, args: ['index-change',] },],
        "onResize": [{ type: core_1.HostListener, args: ['window:resize', ['$event'],] },],
    };
    return CarouselComponent;
}());
exports.CarouselComponent = CarouselComponent;
//# sourceMappingURL=carousel.component.js.map