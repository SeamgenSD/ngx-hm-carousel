"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GALLERY_TYPE;
(function (GALLERY_TYPE) {
    GALLERY_TYPE["YOUTUBE"] = "youtube";
    GALLERY_TYPE["IMG"] = "img";
    GALLERY_TYPE["VIDEO"] = "video";
})(GALLERY_TYPE = exports.GALLERY_TYPE || (exports.GALLERY_TYPE = {}));
//# sourceMappingURL=gallery-item.js.map