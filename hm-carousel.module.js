"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("hammerjs");
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var carousel_component_1 = require("./carousel.component");
var carousel_item_directive_1 = require("./carousel-item.directive");
var safe_pipe_1 = require("./pipe/safe.pipe");
var HmCarouselModule = (function () {
    function HmCarouselModule() {
    }
    HmCarouselModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [
                        common_1.CommonModule,
                        forms_1.FormsModule,
                    ],
                    declarations: [
                        carousel_component_1.CarouselComponent,
                        carousel_item_directive_1.CarouselItemDirective,
                        safe_pipe_1.SafePipe
                    ],
                    exports: [
                        carousel_component_1.CarouselComponent,
                        carousel_item_directive_1.CarouselItemDirective,
                        safe_pipe_1.SafePipe
                    ]
                },] },
    ];
    /** @nocollapse */
    HmCarouselModule.ctorParameters = function () { return []; };
    return HmCarouselModule;
}());
exports.HmCarouselModule = HmCarouselModule;
//# sourceMappingURL=hm-carousel.module.js.map